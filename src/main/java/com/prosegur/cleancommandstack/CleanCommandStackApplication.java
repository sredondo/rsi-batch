package com.prosegur.cleancommandstack;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAutoConfiguration
public class CleanCommandStackApplication implements CommandLineRunner{

	private static Log LOGGER = LogFactory.getLog(CleanCommandStackApplication.class);
	@Autowired private CleanService cleanService;

	public static void main(String[] args) {
		SpringApplication.run(CleanCommandStackApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		LOGGER.info("Ejecutando el borrado de comandos al panel huerfanos");
		cleanService.delete();
		LOGGER.info("Finalizo correctamente");
	}


}
