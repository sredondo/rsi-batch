package com.prosegur.cleancommandstack;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;




@Component
public class CleanService {

    private static Log LOGGER = LogFactory.getLog(CleanService.class);

    @Autowired JdbcTemplate jdbcTemplate;

    @Value("${batch.sql}") private String sql;

    public void delete(){
        try {
            int rows = jdbcTemplate.update(this.sql);
            LOGGER.info("Se borraron " + rows + "correctamente");
        }catch(Throwable t){
            LOGGER.error(t);
        }
    }

}
