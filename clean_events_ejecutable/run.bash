#!/usr/bin/env bash

export APP_PATH=$PWD

# Edit crontab and add:
# 30 * * * * $APP_PATH/run.bash
export JAVA_HOME=/opt/weblogic/java

/opt/weblogic/java/bin/java -jar $APP_PATH/CleanCommandStack-0.0.1-SNAPSHOT.jar

chmod +x slack.sh

this_is_a_title="Comand stack clean events"
this_is_a_message_body="OK, Clean events finalized in well form. You can see complete log on log server"
sulhome=""
slack_hook_url=""

./slack.sh -t $this_is_a_title -b $this_is_a_message_body -c $sulhome -u $slack_hook_url

